//*****************************************************************************
//   +--+
//   | ++----+
//   +-++    |
//     |     |
//   +-+--+  |
//   | +--+--+
//   +----+    Copyright (c) 2011 Code Red Technologies Ltd.
//
// LED flashing SysTick application for LPCXPresso11U14 board
//
// Software License Agreement
//
// The software is owned by Code Red Technologies and/or its suppliers, and is
// protected under applicable copyright laws.  All rights are reserved.  Any
// use in violation of the foregoing restrictions may subject the user to criminal
// sanctions under applicable laws, as well as to civil liability for the breach
// of the terms and conditions of this license.
//
// THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
// OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
// USE OF THIS SOFTWARE FOR COMMERCIAL DEVELOPMENT AND/OR EDUCATION IS SUBJECT
// TO A CURRENT END USER LICENSE AGREEMENT (COMMERCIAL OR EDUCATIONAL) WITH
// CODE RED TECHNOLOGIES LTD.
//
//*****************************************************************************

#include <stdint.h>

#ifdef __USE_CMSIS
#include "LPC11Uxx.h"
#endif


#include "LPC11Uxx.h"
#include "gpio.h"
#include "timer16.h"

#define ON 0
#define OFF 1

#define IN 0
#define OUT 1

#define STOP 0
#define START 1
#define RESET 2
#define NOACTION 3

void delay(uint32_t ms){
	for(uint32_t i=0; i<ms;i+=2)
	{
	//	printf("delay~~~");
	}
}

void setPins(){
 	     //0 intrare, 1 iesire
	GPIOSetDir(0, 9, OUT); //LED este de iesire
	GPIOSetDir(0, 1, OUT); //LED este de iesire
	GPIOSetDir(0, 2, OUT); //LED este de iesire
	GPIOSetDir(0, 3, OUT); //LED este de iesire
	GPIOSetDir(0, 4, OUT); //LED este de iesire
	GPIOSetDir(0, 5, OUT); //LED este de iesire
	GPIOSetDir(0, 7, OUT); //LED este de iesire

    //0 intrare, 1 iesire
	GPIOSetDir(0, 21, IN); // Btn este de intrare
	GPIOSetDir(1, 23, IN); // Btn este de intrare
	GPIOSetDir(0, 11, IN); // Btn este de intrare
}

void displayNumber(int number){
	switch(number){
		case 0:
			GPIOSetBitValue(0,9, ON);
			GPIOSetBitValue(0,1, ON);
			GPIOSetBitValue(0,2, ON);
			GPIOSetBitValue(0,3, ON);
			GPIOSetBitValue(0,4, ON);
			GPIOSetBitValue(0,5, ON);
			GPIOSetBitValue(0,7, OFF);
			break;
		case 1:
			GPIOSetBitValue(0,9, OFF);
			GPIOSetBitValue(0,1, ON);
			GPIOSetBitValue(0,2, ON);
			GPIOSetBitValue(0,3, OFF);
			GPIOSetBitValue(0,4, OFF);
			GPIOSetBitValue(0,5, OFF);
			GPIOSetBitValue(0,7, OFF);
			break;
		case 2:
			GPIOSetBitValue(0,9, ON);
			GPIOSetBitValue(0,1, ON);
			GPIOSetBitValue(0,2, OFF);
			GPIOSetBitValue(0,3, ON);
			GPIOSetBitValue(0,4, ON);
			GPIOSetBitValue(0,5, OFF);
			GPIOSetBitValue(0,7, ON);
			break;
		case 3:
			GPIOSetBitValue(0,9, ON);
			GPIOSetBitValue(0,1, ON);
			GPIOSetBitValue(0,2, ON);
			GPIOSetBitValue(0,3, ON);
			GPIOSetBitValue(0,4, OFF);
			GPIOSetBitValue(0,5, OFF);
			GPIOSetBitValue(0,7, ON);
			break;
		case 4:
			GPIOSetBitValue(0,9, OFF);
			GPIOSetBitValue(0,1, OFF);
			GPIOSetBitValue(0,2, ON);
			GPIOSetBitValue(0,3, OFF);
			GPIOSetBitValue(0,4, OFF);
			GPIOSetBitValue(0,5, ON);
			GPIOSetBitValue(0,7, ON);
			break;
		case 5:
			GPIOSetBitValue(0,9, ON);
			GPIOSetBitValue(0,1, OFF);
			GPIOSetBitValue(0,2, ON);
			GPIOSetBitValue(0,3, ON);
			GPIOSetBitValue(0,4, OFF);
			GPIOSetBitValue(0,5, ON);
			GPIOSetBitValue(0,7, ON);
			break;
		case 6:
			GPIOSetBitValue(0,9, ON);
			GPIOSetBitValue(0,1, OFF);
			GPIOSetBitValue(0,2, ON);
			GPIOSetBitValue(0,3, ON);
			GPIOSetBitValue(0,4, ON);
			GPIOSetBitValue(0,5, ON);
			GPIOSetBitValue(0,7, ON);
			break;
		case 7:
			GPIOSetBitValue(0,9, ON);
			GPIOSetBitValue(0,1, ON);
			GPIOSetBitValue(0,2, ON);
			GPIOSetBitValue(0,3, OFF);
			GPIOSetBitValue(0,4, OFF);
			GPIOSetBitValue(0,5, OFF);
			GPIOSetBitValue(0,7, OFF);
			break;
		case 8:
			GPIOSetBitValue(0,9, ON);
			GPIOSetBitValue(0,1, ON);
			GPIOSetBitValue(0,2, ON);
			GPIOSetBitValue(0,3, ON);
			GPIOSetBitValue(0,4, ON);
			GPIOSetBitValue(0,5, ON);
			GPIOSetBitValue(0,7, ON);
			break;
		case 9:
			GPIOSetBitValue(0,9, ON);
			GPIOSetBitValue(0,1, ON);
			GPIOSetBitValue(0,2, ON);
			GPIOSetBitValue(0,3, ON);
			GPIOSetBitValue(0,4, OFF);
			GPIOSetBitValue(0,5, ON);
			GPIOSetBitValue(0,7, ON);
			break;
		default:
			GPIOSetBitValue(0,9, OFF);
			GPIOSetBitValue(0,1, OFF);
			GPIOSetBitValue(0,2, OFF);
			GPIOSetBitValue(0,3, OFF);
			GPIOSetBitValue(0,4, OFF);
			GPIOSetBitValue(0,5, OFF);
			GPIOSetBitValue(0,7, OFF);
			break;
	}
}

void getInput(uint32_t* cmd){
	  if (!GPIOGetPinValue(0,21))
	  {
		  *cmd = STOP;
	  }else if(!GPIOGetPinValue(1,23)){
		  *cmd = START;
	  }else if(!GPIOGetPinValue(0,11)){
		  *cmd =RESET;
	  }
}


int main(void) {
	  GPIOInit();
	  setPins();

	  uint32_t n = 9;
	  uint32_t cmd;
	  while (1){
		  getInput(&cmd);
		  if(cmd == RESET){
			   n = 9;
		  }
		  if(cmd == START){
			  if(n > 0 ){
				  n-=1;
				  delay(500000);
			  }
		  }
		  displayNumber(n);
	  } //end while
}

